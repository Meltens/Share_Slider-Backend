# Share_Slider Backend
> This is backend for Share_Slider

## How to use

~~~sh
$ touch .env

$ docker-compose run web bundle install --path vendor/bundle --without production

$ docker-compose run web bundle exec rake db:create

$ docker-compose run web bundle exec rake db:migrate

$ docker-compose up
~~~

## What is Share_Slider

Share_Slider is real time photo share service for event

[Frontend](https://gitlab.com/Meltens/Photo-Sider)

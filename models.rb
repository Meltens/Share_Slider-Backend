require 'bundler/setup'
Bundler.require
require 'net/http'
require 'uri'
require 'json'
require 'redis'
require 'hiredis'

# Database
@config = YAML.load_file('./database.yml')
ActiveRecord::Base.configurations = @config

if development?
  ActiveRecord::Base.establish_connection(@config['development'])
else
  ActiveRecord::Base.establish_connection(@config['production'])
end

# Time zone
Time.zone = 'Tokyo'
ActiveRecord::Base.default_timezone = :local

after do
  ActiveRecord::Base.connection.close
end

# Room is ..
class Room < ActiveRecord::Base
  has_secure_password
  validates :uniq_id, uniqueness: true
end

# Redis is ...
class TooManyRequest
  def initialize
    @config = YAML.load_file('./database.yml')['redis']
    @redis  = Redis.new(@config)
  end

  def confirmation(host_name)
    logs = @redis.get(host_name)
    if logs
      logs = JSON.parse(logs)
      logs << Time.now.to_i
      @redis.set(host_name, logs.to_json)
    else
      logs = [Time.now.to_i]
      @redis.set(host_name, logs.to_json)
    end

    max  = ENV['R_MAX']  || 5
    time = ENV['R_TIME'] || -30
    sum  = 0
    len  = logs.length
    if len > max
      for i in (len-max)..(len-2)
        num = logs[i] - logs[i+1]
        sum += num
      end
      return sum <= time
    else
      return true
    end
  end
end

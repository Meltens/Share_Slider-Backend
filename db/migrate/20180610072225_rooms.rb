# Rooms is ...
class Rooms < ActiveRecord::Migration[5.1]
  def change
    create_table :rooms do |t|
      t.string :name, null: false
      t.string :password
      t.string :uniq_id, null: false
      t.timestamps null: false
    end
  end
end

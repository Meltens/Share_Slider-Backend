post '/room' do # get room data
  result = TooManyRequest.new.confirmation(request.env['REMOTE_HOST'])
  if result
    receive_data = JSON.parse(request.body.read)
    room = Room.find_by(uniq_id: receive_data['uniq_id'])
    if room
      if room.authenticate(receive_data['passowrd'])
        body(room.to_json)
        status 200
      else
        status 401
      end
    else
      status 404
    end
  else
    status 429
  end
end

delete '/rooms/:uniq_id' do
  room = Room.find_by(uniq_id: params[:uniq_id])
  if room
    room.delete
  else
    status 404
  end
end

post '/rooms' do
  room = Room.new(JSON.parse(request.body.read))
  if room
    room.uniq_id = SecureRandom.uuid
    if room.save
      body({
        name: room.name,
        uniq_id: room.uniq_id
      }.to_json)
      status 201
    else
      status 500
    end
  else
    status 400
  end
end

put '/rooms/:uniq_id' do
  room = Room.find_by(uniq_id: params[:uniq_id])
  if room
    room.update(JSON.parse(request.body.read))
  else
    status 404
  end
end
